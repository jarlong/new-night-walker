﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundShuffle : MonoBehaviour {

    private AudioSource source;


	// Use this for initialization
	void Start () {
        source = GetComponent<AudioSource>();
        source.volume = Random.Range(0.4f, 0.6f);
        source.pitch = Random.Range(0.8f, 1.1f);
    }
	
}
