﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class easterEgg : MonoBehaviour {

    public GameObject easterEggScreen;
    public GameObject offScreen;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            easterEggScreen.SetActive(true);
            offScreen.SetActive(false);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            easterEggScreen.SetActive(true);
            offScreen.SetActive(false);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            easterEggScreen.SetActive(false);
            offScreen.SetActive(true);
        }
    }
}
