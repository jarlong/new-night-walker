﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class isBush : MonoBehaviour {

    public AudioClip bushSound;
    public AudioClip clipHold;
    private AudioSource source;
    private foosteps bushStep;
	// Use this for initialization
	void Start () {
        source = GameObject.FindGameObjectWithTag("Player").GetComponent<AudioSource>();
        bushStep = GameObject.FindGameObjectWithTag("Player").GetComponent<foosteps>();
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            clipHold = source.clip;
            source.clip = bushSound;
            source.volume = 0.1f;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            source.clip = bushSound;
            source.volume = 0.1f;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            source.clip = clipHold;
        }
    }
}
